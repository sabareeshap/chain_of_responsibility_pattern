"""Tests to verify the chain design is working as expected."""

from collections.abc import Iterable
from unittest import TestCase, mock

from base import (
    HandlerContext,
    AbstractHandler,
    ConditionalHandler,
    DuplicateHandlerException,
    EndOfChain,
    SkipNextChain,
    StopChain,
)


class SimpleHandler(AbstractHandler):
    def handle(self, context):
        return True


class SimpleConditionalHandler(ConditionalHandler):
    def handle(self, context):
        return context["success"]


class TestHandlerContext(TestCase):
    """Tests to check functionalities in HandleContext is working as expected."""

    def test_initialization_with_invalid_params_fail(self):
        """Test to verify that context initialization fails with invalid params."""

        self.assertRaises(TypeError, HandlerContext)  # without any params

        with self.assertRaises(TypeError):
            context = HandlerContext(None)

        with self.assertRaises(TypeError):
            # must be instance, not class
            context = HandlerContext(AbstractHandler)

    def test_initialization_success(self):
        """Test to check handler context initialization is successful"""

        handler = SimpleHandler()

        ctx = HandlerContext(handler)
        self.assertTrue(isinstance(ctx, HandlerContext))
        self.assertEqual(ctx.root_handler, handler)
        self.assertIn(handler, ctx.handlers)
        self.assertIn(handler, ctx._queue)
        self.assertEqual(ctx.last_consumed_handler, None)

    def test_register_handler_success(self):
        """Test handlers register to an existing context"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()

        ctx = HandlerContext(handler1)
        ctx.register_handler(handler2)

        self.assertIn(handler2, ctx.handlers)
        self.assertIn(handler2, ctx._queue)

    def test_register_duplicate_handler_fail(self):
        """Test handlers register to an existing context"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()

        ctx = HandlerContext(handler1)
        ctx.register_handler(handler2)

        self.assertRaises(DuplicateHandlerException, ctx.register_handler, handler2)

    def test_registered_handlers_order_in_the_queue(self):
        """Test registered handlers order in the queue is first to last,
        also making sure root handlers is always at first."""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        expected_queue_order = [handler1, handler2, handler3]

        ctx = HandlerContext(handler1)
        ctx.register_handler(handler2)
        ctx.register_handler(handler3)

        self.assertEqual(expected_queue_order, list(ctx._queue))
        self.assertEqual(handler1, ctx.root_handler)

    def test_registered_handlers_have_back_and_fourth_relation(self):
        """Test registered handlers have back and fourth relation"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        ctx = HandlerContext(handler1)
        ctx.register_handler(handler2)
        ctx.register_handler(handler3)

        self.assertEqual(handler1, handler2._previous_handler)
        self.assertEqual(handler2, handler1._next_handler)
        self.assertEqual(handler3, handler2._next_handler)

    def test_consume_handler(self):
        """Test handlers are consumed from first to last begining with root handler"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        ctx = HandlerContext(handler1)
        ctx.register_handler(handler2)
        ctx.register_handler(handler3)

        consumed_handler1 = ctx.consume_handler()

        self.assertEqual(ctx.root_handler, consumed_handler1)
        self.assertEqual(list(ctx._queue), [handler2, handler3])

        consumed_handler2 = ctx.consume_handler()
        consumed_handler3 = ctx.consume_handler()

        self.assertEqual(len(ctx._queue), 0)  # queue empty
        self.assertEqual(handler1, consumed_handler1)
        self.assertEqual(handler2, consumed_handler2)
        self.assertEqual(handler3, consumed_handler3)

    def test_consume_handler_on_empty_queue_raises(self):
        """Test consume handler on empty queue raises `EndOfChain` exception"""

        handler = SimpleHandler()
        ctx = HandlerContext(handler)
        _consumed_handler = ctx.consume_handler()

        self.assertEqual(len(ctx._queue), 0)
        self.assertRaises(EndOfChain, ctx.consume_handler)

    def test_peek_queue_method(self):
        """Test peek queue method return a iterable of items in queue"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()

        ctx = HandlerContext(handler1)
        ctx.register_handler(handler2)

        self.assertTrue(ctx.peek_queue(), Iterable)
        self.assertEqual(list(ctx._queue), list(ctx.peek_queue()))
        _ = ctx.consume_handler()
        self.assertEqual(list(ctx._queue), list(ctx.peek_queue()))

    def test_peek_queue_in_reversed(self):
        """Test peek queue can return reversed result"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        ctx = HandlerContext(handler1)
        ctx.register_handler(handler2)
        ctx.register_handler(handler3)

        expected_result = [handler3, handler2, handler1]
        self.assertTrue(ctx.peek_queue(reverse=True), Iterable)
        self.assertEqual(expected_result, list(ctx.peek_queue(reverse=True)))

        _ = ctx.consume_handler()
        expected_result2 = [handler3, handler2]
        self.assertEqual(expected_result2, list(ctx.peek_queue(reverse=True)))

    def test_register_handler_before_root_fail(self):
        """Test register handler to start of queue before root fail"""

        root_handler = SimpleHandler()
        normal_handler = SimpleHandler()
        priority_handler = SimpleHandler()

        ctx = HandlerContext(root_handler)
        ctx.register_handler(normal_handler)
        with self.assertRaises(ValueError):
            ctx.register_handler(priority_handler, prior=True)

    def test_register_handler_to_partially_consumed_queue_success(self):
        """Test registering handler after root handler consumed is success"""

        root_handler = SimpleHandler()
        normal_handler = SimpleHandler()
        priority_handler = SimpleHandler()

        ctx = HandlerContext(root_handler)
        ctx.register_handler(normal_handler)

        _consumed = ctx.consume_handler()
        ctx.register_handler(priority_handler, prior=True)
        expected_result = [priority_handler, normal_handler]
        self.assertEqual(expected_result, list(ctx._queue))


class TestAbstractHandler(TestCase):

    def test_abstract_handler(self):
        """Test instance of `AbstractHandler` cannot be created."""

        self.assertRaises(TypeError, AbstractHandler)

    def test_abstract_method_overridden(self):
        """Test instance of child class cannot be created
        if abstractmethod `handle` is not overridden"""

        class ChildHandler(AbstractHandler):
            pass

        self.assertRaises(TypeError, ChildHandler)

    def test_initialization_success(self):
        """Test successfully able to create a child handler"""

        class ChildHandler(AbstractHandler):
            def handle(self, context):
                pass

        instance = ChildHandler()
        self.assertTrue(isinstance(instance, ChildHandler))
        self.assertTrue(isinstance(instance, AbstractHandler))

        self.assertTrue(hasattr(instance, "_ctx"))
        self.assertTrue(isinstance(instance._ctx, HandlerContext))
        self.assertEqual(instance._previous_handler, None)
        self.assertEqual(instance._next_handler, None)
        self.assertTrue(hasattr(instance, "_handle_state"))

    def test_conditional_handler_is_one_of_abstract_class(self):
        """Test conditional handler is of type AbstractHandler
        and contains the same properties"""

        self.assertTrue(issubclass(ConditionalHandler, AbstractHandler))

    def test_initialization_of_handler(self):
        """Test new handler initialization is as expected"""

    def test_register_handler_to_chain(self):
        """Test `set_next` method register handler to a single chain"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        handler1.set_next(handler2).set_next(handler3)

        expected_queue = [handler1, handler2, handler3]

        # checking contexts of all handlers are same, since they registered
        self.assertEqual(handler1._ctx, handler2._ctx)
        self.assertEqual(handler2._ctx, handler3._ctx)

        ctx = handler1._ctx  # all handlers context are same ^
        self.assertEqual(list(ctx.peek_queue()), expected_queue)

    def test_first_handler_in_the_chain_is_root(self):
        """Test first handler is the root handler"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        handler1_ctx = handler1._ctx
        handler2_ctx = handler2._ctx
        handler3_ctx = handler3._ctx

        chain = handler1.set_next(handler2).set_next(handler3)
        ctx = chain._ctx  # common context

        self.assertEqual(ctx, handler1_ctx)
        self.assertNotEqual(ctx, handler2_ctx)
        self.assertNotEqual(ctx, handler3_ctx)

    def test_create_chain_from_sub_chain_success(self):
        """Test create chain from multiple sub chain is success"""

        sub1 = SimpleHandler()
        sub2 = SimpleHandler()
        sub3 = SimpleHandler()
        sub4 = SimpleHandler()
        sub5 = SimpleHandler()

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()

        sub_chain1 = sub1.set_next(sub2)
        sub_chain2 = sub3.set_next(sub4).set_next(sub5)

        main_chain = (
            handler1.set_next(sub_chain1).set_next(handler2).set_next(sub_chain2)
        )

        ctx = main_chain._ctx

        expected_queue = [handler1, sub1, sub2, handler2, sub3, sub4, sub5]
        self.assertEqual(list(ctx.peek_queue()), expected_queue)
        self.assertEqual(handler1, ctx.root_handler)

    def test_duplicate_chaining_raises_error(self):
        """Test that duplicate chaining raises error"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()

        chain = handler1.set_next(handler2)

        self.assertRaises(DuplicateHandlerException, chain.set_next, handler2)

    def test_next_property(self):
        """Test property `next` returns next handler in the chain."""
        handler1 = SimpleHandler()
        handler2 = SimpleHandler()

        chain = handler1.set_next(handler2)
        self.assertEqual(handler1.next, handler2)
        self.assertEqual(handler2.previous, handler1)

    def test_previous_property(self):
        """Test property `previous` returns previous handler in the chain."""
        handler1 = SimpleHandler()
        handler2 = SimpleHandler()

        chain = handler1.set_next(handler2)
        self.assertEqual(handler2.previous, handler1)

    @mock.patch.object(SimpleHandler, "handle")
    def test_handler_execute_success(self, mocked_handle):
        """Test chain execute run all handle method of registered handlers in order"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        chain = handler1.set_next(handler2).set_next(handler3)
        chain.execute(context=None)

        self.assertEqual(mocked_handle.call_count, 3)

    def test_handler_execute_in_the_order_success(self):
        """Test chain execute run all handle method of registered handlers in order"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        def func_handle1(ctx: list):
            ctx.append(1)

        def func_handle2(ctx: list):
            ctx.append(2)

        def func_handle3(ctx: list):
            ctx.append(3)

        chain = handler1.set_next(handler2).set_next(handler3)

        with (
            mock.patch.object(handler1, "handle", side_effect=func_handle1),
            mock.patch.object(handler2, "handle", side_effect=func_handle2),
            mock.patch.object(handler3, "handle", side_effect=func_handle3),
        ):

            result = chain.execute(context=[])
            self.assertEqual(result, [1, 2, 3])

    def test_handler_skip_next_success(self):
        """Test it is possible to skip next handler from an handler during execution"""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        def func_handle1(ctx: list):
            ctx.append(1)
            raise SkipNextChain

        def func_handle2(ctx: list):
            ctx.append(2)  # this will be skipped

        def func_handle3(ctx: list):
            ctx.append(3)

        chain = handler1.set_next(handler2).set_next(handler3)
        expected_output = [1, 3]  # skipped handler2

        with (
            mock.patch.object(handler1, "handle", side_effect=func_handle1),
            mock.patch.object(handler2, "handle", side_effect=func_handle2),
            mock.patch.object(handler3, "handle", side_effect=func_handle3),
        ):

            result = chain.execute(context=[])
            self.assertEqual(result, expected_output)

    def test_handler_stop_chain_success(self):
        """Test it is possible to stop the chain execution from one handler."""

        handler1 = SimpleHandler()
        handler2 = SimpleHandler()
        handler3 = SimpleHandler()

        def func_handle1(ctx: list):
            ctx.append(1)
            raise StopChain

        def func_handle2(ctx: list):
            ctx.append(2)  # this will be skipped

        def func_handle3(ctx: list):
            ctx.append(3)

        chain = handler1.set_next(handler2).set_next(handler3)
        expected_output = [1]  # stopped handler2, handler3

        with (
            mock.patch.object(handler1, "handle", side_effect=func_handle1),
            mock.patch.object(handler2, "handle", side_effect=func_handle2),
            mock.patch.object(handler3, "handle", side_effect=func_handle3),
        ):

            result = chain.execute(context=[])
            self.assertEqual(result, expected_output)

    def test_conditional_chaining(self):
        """Test conditional chaining during"""
