from abc import ABC, abstractmethod
from typing import Optional, Iterable
from enum import Enum
from collections import deque


class ChainState(Enum):
    END = "end of chain"
    START = "start of chain"
    COMPLETED = "execution completed"


class MethodNotOverriddenException(Exception):
    """This method needs to be overridden by subclass"""


class DuplicateHandlerException(Exception):
    """This handler is already registered"""


class StartOfChain(Exception):
    """This is the start of the chain, and has no previous handlers assigned."""

    pass


class EndOfChain(Exception):
    """This is the end of the chain, and has no next handlers assigned."""

    pass


class SkipNextChain(Exception):
    """This tells the handler to skip the upcoming chain"""

    pass


class StopChain(Exception):
    """This tells the handler to stop the chain execution"""

    pass


class HandlerContext:
    """Context of the chain will be maintained in this class."""

    def __init__(self, root_handler: "AbstractHandler"):
        if not isinstance(root_handler, AbstractHandler):
            raise TypeError(f"Must be of type '{AbstractHandler}")

        self.root_handler = root_handler
        self.handlers = {root_handler}
        self._queue = deque([root_handler])
        # if not isinstance(root_handler, EmptyHandler)
        # else deque()
        root_handler._queue_position = 0
        self.last_consumed_handler = None

    def register_handler(self, handler: "AbstractHandler", prior: bool = False):
        """Register a new handler to this chain context.

        Raises:
            DuplicateHandlerException: If handler is already registered to this context.

        """

        handler_ctx = handler._ctx
        if prior is True:
            self.prepend_handlers_to_queue(handler_ctx)
        else:
            self.append_handlers_to_queue(handler_ctx)

    def append_handlers_to_queue(self, handler_ctx):
        """Append all handlers from their unique context to this context's end."""

        parent = self._queue[-1]
        while handler_ctx._queue:
            handler_x = handler_ctx._queue.popleft()
            self._register(handler_x)
            self._queue.append(handler_x)

        self.link_handlers(handler_ctx.root_handler, parent)
        del handler_ctx

    def prepend_handlers_to_queue(self, handler_ctx):
        """Prepend all handlers from their unique context to this context's beginning.

        This functionality is only viable to use during chain execution,
        as it requires at least 1 of the handler to be consumed in this
        main context (ie; root_handler).

        Raises:
            ValueError: When tried to insert before the root handler of this
            context (ie; before any handler being consumed).

        """

        if not self.last_consumed_handler:
            # root handler is not consumed
            raise ValueError("Cannot append handler before root handler")

        if self._queue:
            self.link_handlers(self._queue[0], handler_ctx._queue[-1])

        while handler_ctx._queue:
            end_handler = handler_ctx._queue.pop()
            self._register(end_handler)
            self._queue.appendleft(end_handler)
        self.link_handlers(handler_ctx.root_handler, self.last_consumed_handler)
        del handler_ctx

    def link_handlers(self, child, parent):
        """Add a linking between parent and child handler"""

        child._previous_handler = parent
        parent._next_handler = child

    def peek_queue(self, reverse=False):
        """Returns iterable containing available handlers in the queue."""

        queue_items = reversed(self._queue) if reverse else self._queue
        for h in queue_items:
            yield h

    def consume_handler(self):
        """Consumes a handler from the left side of queue."""

        try:
            self.last_consumed_handler = self._queue.popleft()
            return self.last_consumed_handler
        except IndexError:
            raise EndOfChain

    def consume_root_handler(self):
        """Consumes the root handler from the start of queue.

        Raises:
            ValueError: If root handler is already consumed

        """
        handler = self.consume_handler()
        if handler == self.root_handler:
            return handler
        raise ValueError("Root already consumed")

    def _register(self, handler):
        """Register an handler to this context.

        Raises:
            DuplicateHandlerException: If handler is already registered

        """

        if handler in self.handlers:
            # preventing from using the chain, developer need to re-create the chain,
            # resolving the issue.
            self._queue = None
            raise DuplicateHandlerException(
                f"Handler <{handler}> is already registered"
            )
        handler._ctx = self  # update handler context
        self.handlers.add(handler)


class AbstractHandler(ABC):
    """Abstract class for handler"""

    _previous_handler: "AbstractHandler" = None
    _next_handler: "AbstractHandler" = None
    _queue_position: int = None
    _ctx: HandlerContext = None

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        instance._handle_state = None
        instance._ctx = HandlerContext(instance)
        return instance

    @abstractmethod
    def handle(self, context):

        # This method must be overridden in from extending class
        # and it should contain the logic to handle the context.
        raise NotImplementedError

    @property
    def next(self):
        handler = self._next_handler
        if not handler:
            raise EndOfChain
        return handler

    @property
    def previous(self):
        handler = self._previous_handler
        if not handler:
            raise StartOfChain
        return handler

    def set_next(self, handler: "AbstractHandler") -> "AbstractHandler":
        """Add a handler or chain of handlers to same chain that will execute after this."""
        # Set a child handler that should execute after this.
        # Also override the root of the handler and set it to the root of this
        # current handler.

        assert isinstance(
            handler, AbstractHandler
        ), f"Invalid handler type provided <{type(handler)}>"

        self._ctx.register_handler(handler)

        return self

    def _handle(self, context):
        try:
            self._handle_state = self.handle(context)
        except SkipNextChain:
            # checkout other way to skip handlers
            skipped = self._ctx.consume_handler()
        except StopChain:
            return

        # event : chain status

        try:
            # TODO: move to _execute method
            handler = self.get_next_handler()
            handler._handle(context)
        except EndOfChain:
            pass

    def get_next_handler(self) -> "AbstractHandler":
        return self._ctx.consume_handler()

    def execute(self, context):
        """Method to start the chain from root"""
        self._ctx.consume_root_handler()._handle(context)
        return context


class ConditionalHandler(AbstractHandler):

    def __init__(self, *, success=None, failure=None):
        self.success_handler = success
        self.failure_handler = failure

        assert isinstance(success, AbstractHandler) or isinstance(
            failure, AbstractHandler
        )

    def get_next_handler(self) -> AbstractHandler:
        handler = self.success_handler if self._handle_state else self.failure_handler
        if handler:
            self._ctx.register_handler(handler, prior=True)
        return super().get_next_handler()


class DummyChainHandler(AbstractHandler):

    def set_next(self, handler: AbstractHandler) -> AbstractHandler:
        # This handler is simply a dummy and it will just replicate the
        # behavior of the handler passed to this method.
        # One Useful scenario is while initiate root handler when the chain
        # is initially unknown.
        assert isinstance(
            handler, AbstractHandler
        ), f"Invalid handler type provided <{type(handler)}>"
        self.__class__ = handler.__class__
        self.__dict__ = handler.__dict__
        return handler

    def handle(self, context):
        pass
