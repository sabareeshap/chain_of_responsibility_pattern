from base import AbstractHandler, ConditionalHandler, SkipNextChain


class PreCheckChainHandler(AbstractHandler):
    def handle(self, context):
        print("Pre check")


class IPCheckChainHandler(AbstractHandler):
    def handle(self, context):
        print("IP check")


class LocationCheckChainHandler(AbstractHandler):
    def handle(self, context):
        print("Location check")


class CheckPreviousApplicationChainHandler(ConditionalHandler):
    def handle(self, context):
        exists = context["previous_exists"]
        if exists:
            print("Found previous customer application..")
        else:
            print("No previous application found..")
        return exists


class BankAPIPanValidationChainHandler(AbstractHandler):
    def handle(self, context):
        print("Validating pan details")


class GenerateVKYCLinkChainHandler(AbstractHandler):
    def handle(self, context):
        print("Generating new link for the video kyc")


# new application
class CreateNewApplicationChainHandler(AbstractHandler):
    def handle(self, context):
        print("Creating new KYC application for the customer")


# update application
class CheckApplicationAlreadyUnderProcessChainHandler(AbstractHandler):
    def handle(self, context):
        print("Checking whether application is already under process")


class BackupPreviousCustomerInformationChainHandler(AbstractHandler):
    def handle(self, context):
        print(
            "Backing up previous customer application information to customer attempts."
        )


class UpdateExistingApplicationChainHandler(AbstractHandler):
    def handle(self, context):
        print("Update existing customer application with available data")


