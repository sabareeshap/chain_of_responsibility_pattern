from abc import ABC, abstractmethod
from typing import Optional
from enum import Enum


class ChainState(Enum):
    END = "end of chain"
    START = "start of chain"
    COMPLETED = "execution completed"


class MethodNotOverriddenException(Exception):
    """This method needs to be overridden by subclass"""


class DuplicateHandlerException(Exception):
    """This handler is already registered"""


class StartOfChain(Exception):
    """This is the start of the chain, and has no previous handlers assigned."""

    pass


class EndOfChain(Exception):
    """This is the end of the chain, and has no next handlers assigned."""

    pass


class SkipNextChain(Exception):
    """This tells the handler to skip the upcoming chain"""

    pass


class StopChain(Exception):
    """This tells the handler to stop the chain execution"""

    pass


class HandlerContext:
    def __init__(self, root_handler):
        self.root_handler = root_handler
        self.children = {}

    def register_handler(self, handler):
        if self.children.get(handler):
            raise DuplicateHandlerException(
                f"Handler <{handler}> is already registered"
            )
        self.children[handler] = handler
      
    


class AbstractHandler(ABC):

    _next_handler: "AbstractHandler" = None  # end of state
    _previous_handler: "AbstractHandler" = None  # start of state

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        instance._root = instance
        instance._handle_state = None
        instance._ctx = HandlerContext(instance)
        return instance

    @abstractmethod
    def handle(self, context):
        # This method must be overridden in from extending class
        # and it should contain the logic to handle the context.
        pass

    def set_next(self, handler: "AbstractHandler") -> "AbstractHandler":
        # Set a child handler that should execute after this.
        # Also override the root of the handler and set it to the root of this
        # current handler.

        assert isinstance(
            handler, AbstractHandler
        ), f"Invalid handler type provided <{type(handler)}>"

        self._ctx.register_handler(handler)

        self._next_handler = handler
        handler._root = self._root
        handler._previous_handler = self

        return handler

    def _handle(self, context):
        skip_next = False
        if self._handle_state == ChainState.COMPLETED:
            raise
        try:
            self._handle_state = self.handle(context)
        except SkipNextChain:
            skip_next = True
        finally:
            self._handle_state = ChainState.COMPLETED

        try:
            handler = self.get_next_handler(skip_next)
            self._propagate_next_handler(handler, context)
        except EndOfChain:
            pass

    def _propagate_next_handler(self, handler, context):
        handler._handle(context)

    def get_next_handler(self, skip_one=False) -> "AbstractHandler":
        if not self._next_handler:
            raise EndOfChain
        if skip_one:
            return self._next_handler.get_next_handler()
        return self._next_handler

    def execute(self, context):
        try:
            self._root._handle(context)
        except StopChain:
            pass


class ConditionalHandler(AbstractHandler):

    def __init__(self, *, success=None, failure=None):
        self.success_handler = success
        self.failure_handler = failure

        assert isinstance(success, AbstractHandler) or isinstance(
            failure, AbstractHandler
        )

    def handle(self, context) -> Optional[bool]:
        raise MethodNotOverriddenException(
            f"`handle` method is not overridden in {self.__class__.__name__}."
        )

    def get_next_handler(self, skip_one=False) -> AbstractHandler:
        main_handler = self._next_handler
        handler = self.success_handler if self._handle_state else self.failure_handler
        if handler:
            if main_handler:
                handler = handler.set_next(main_handler)
        else:
            handler = main_handler
        self._next_handler = handler
        return super().get_next_handler(skip_one=False)

    def _propagate_next_handler(self, handler, context):
        handler.execute(context)
