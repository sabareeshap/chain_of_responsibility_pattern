from abc import ABC, abstractmethod
from typing import Optional
from enum import Enum
from collections import deque


class ChainState(Enum):
    END = "end of chain"
    START = "start of chain"
    COMPLETED = "execution completed"


class MethodNotOverriddenException(Exception):
    """This method needs to be overridden by subclass"""


class DuplicateHandlerException(Exception):
    """This handler is already registered"""


class StartOfChain(Exception):
    """This is the start of the chain, and has no previous handlers assigned."""

    pass


class EndOfChain(Exception):
    """This is the end of the chain, and has no next handlers assigned."""

    pass


class SkipNextChain(Exception):
    """This tells the handler to skip the upcoming chain"""

    pass


class StopChain(Exception):
    """This tells the handler to stop the chain execution"""

    pass


class HandlerContext:
    def __init__(self, root_handler):
        self.root_handler = root_handler
        self.handlers = {root_handler}
        self._queue = deque([root_handler])
        root_handler._queue_position = 0

    def register_handler(self, handler, parent, prior=False):
        if handler in self.handlers:
            raise DuplicateHandlerException(
                f"Handler <{handler}> is already registered"
            )
        handler._ctx = self
        self.handlers.add(handler)
        if prior:
            if self._queue:
                child_handler = self._queue[0]
                child_handler._previous_handler = handler
                handler._next_handler = child_handler
            else:
                handler._next_handler = None
            self._queue.insert(0, handler)
        else:
            self._queue.append(handler)

        handler._previous_handler = parent
        parent._next_handler = handler

    def peek_queue(self, reverse=False):
        gen = reversed(self._queue) if reverse else self._queue
        for h in gen:
            yield h

    def consume_handler(self):
        try:
            return self._queue.popleft()
        except IndexError:
            raise EndOfChain

    def consume_root_handler(self):
        rh = self.consume_handler()
        if rh == self.root_handler:
            return rh
        raise ValueError("Root already consumed")


class AbstractHandler(ABC):

    _previous_handler: "AbstractHandler" = None
    _next_handler: "AbstractHandler" = None
    _queue_position: int = None
    _ctx: HandlerContext = None

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        instance._handle_state = None
        instance._ctx = HandlerContext(instance)
        return instance

    @abstractmethod
    def handle(self, context):
        # This method must be overridden in from extending class
        # and it should contain the logic to handle the context.
        pass

    @property
    def next(self):
        handler = self._next_handler
        if not handler:
            raise EndOfChain
        return handler

    @property
    def previous(self):
        handler = self._previous_handler
        if not handler:
            raise StartOfChain
        return handler

    def set_next(self, handler: "AbstractHandler") -> "AbstractHandler":
        # Set a child handler that should execute after this.
        # Also override the root of the handler and set it to the root of this
        # current handler.

        assert isinstance(
            handler, AbstractHandler
        ), f"Invalid handler type provided <{type(handler)}>"

        self._ctx.register_handler(handler, self)

        return handler

    def _handle(self, context):
        try:
            self._handle_state = self.handle(context)
        except SkipNextChain:
            skipped = self._ctx.consume_handler()

        try:
            handler = self.get_next_handler()
            handler._handle(context)
        except EndOfChain:
            pass

    def get_next_handler(self) -> "AbstractHandler":
        return self._ctx.consume_handler()

    def execute(self, context):
        """Method to start the chain from root"""
        self._ctx.consume_root_handler()._handle(context)


class ConditionalHandler(AbstractHandler):

    def __init__(self, *, success=None, failure=None):
        self.success_handler = success
        self.failure_handler = failure

        assert isinstance(success, AbstractHandler) or isinstance(
            failure, AbstractHandler
        )

    def handle(self, context) -> Optional[bool]:
        raise MethodNotOverriddenException(
            f"`handle` method is not overridden in {self.__class__.__name__}."
        )

    def get_next_handler(self) -> AbstractHandler:
        handler = self.success_handler if self._handle_state else self.failure_handler
        if handler:
            for h in handler._ctx.peek_queue(reverse=True):
                self._ctx.register_handler(h, self, prior=True)
        return super().get_next_handler()
