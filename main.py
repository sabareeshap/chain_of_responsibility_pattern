import handlers
import yaml
from base import DummyChainHandler


def get_sub_chain(sub_chain, config, channel):
    sub = config[channel].get("chains", {}).get(sub_chain)
    if not sub:
        sub = config["chains"][sub_chain]
    return sub


def connect_chain(chains, config, channel):
    chain = DummyChainHandler()
    if isinstance(chains, list):
        chains = dict.fromkeys(chains)

    for handler_name, extra in chains.items():
        handler = getattr(handlers, handler_name)
        if not extra:
            chain.set_next(handler())
        else:
            params = {}
            for p, sub_chain in extra.items():
                sub = get_sub_chain(sub_chain, config, channel)
                params[p] = connect_chain(sub, config, channel)
            chain.set_next(handler(**params))
    return chain


if __name__ == "__main__":
    with open("chain.yml") as f:
        config = yaml.safe_load(f)

    # print(config)

    channel = "default"

    chain = connect_chain(config[channel]["stages"], config, channel)
    chain.execute(context={"previous_exists": True})
