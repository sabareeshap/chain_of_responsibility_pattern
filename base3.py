from abc import ABC, abstractmethod
from typing import Optional
from enum import Enum


class ChainState(Enum):
    END = "end of chain"
    START = "start of chain"
    COMPLETED = "execution completed"


class MethodNotOverriddenException(Exception):
    """This method needs to be overridden by subclass"""


class DuplicateHandlerException(Exception):
    """This handler is already registered"""


class StartOfChain(Exception):
    """This is the start of the chain, and has no previous handlers assigned."""

    pass


class EndOfChain(Exception):
    """This is the end of the chain, and has no next handlers assigned."""

    pass


class SkipNextChain(Exception):
    """This tells the handler to skip the upcoming chain"""

    pass


class StopChain(Exception):
    """This tells the handler to stop the chain execution"""

    pass


class HandlerContext:
    def __init__(self, root_handler):
        self.root_handler = root_handler
        self.handlers = {root_handler: root_handler}
        self._queue = [root_handler]
        root_handler._queue_position = 0

    def register_handler(self, handler, position=-1):
        if self.handlers.get(handler):
            raise DuplicateHandlerException(
                f"Handler <{handler}> is already registered"
            )
        handler._ctx = self
        self.handlers[handler] = handler
        if position >= 0:
            self._validate_handler_insert_position(position)
            self._queue.insert(position, handler)
            try:
                child_handler = self._queue[position + 1]
                child_handler._previous_handler = handler
                handler._next_handler = child_handler
            except IndexError:
                handler._next_handler = None
            parent_handler = self._queue[position - 1]
            handler._queue_position = position
        else:
            self._queue.append(handler)
            parent_handler = self._queue[-2]
            handler._queue_position = len(self._queue) - 1

        handler._previous_handler = parent_handler
        parent_handler._next_handler = handler

    def _validate_handler_insert_position(self, position):
        max_bound = len(self._queue) - 1
        if position == 0 or position > max_bound:
            raise ValueError(
                f"Invalid position {position}: must be between {self._next_index} and "
                f"{max_bound} and cannot be 0."
            )

    def get_root_handler(self):
        return self.root_handler

    def peek_queue(self):
        return tuple(self._queue)


class AbstractHandler(ABC):

    _previous_handler: "AbstractHandler" = None
    _next_handler: "AbstractHandler" = None
    _queue_position: int = None
    _ctx: HandlerContext = None

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        instance._handle_state = None
        instance._ctx = HandlerContext(instance)
        return instance

    @abstractmethod
    def handle(self, context):
        # This method must be overridden in from extending class
        # and it should contain the logic to handle the context.
        pass

    @property
    def next(self):
        handler = self._next_handler
        if not handler:
            raise EndOfChain
        return handler

    @property
    def previous(self):
        handler = self._previous_handler
        if not handler:
            raise StartOfChain
        return handler

    def set_next(self, handler: "AbstractHandler") -> "AbstractHandler":
        # Set a child handler that should execute after this.
        # Also override the root of the handler and set it to the root of this
        # current handler.

        assert isinstance(
            handler, AbstractHandler
        ), f"Invalid handler type provided <{type(handler)}>"

        self._ctx.register_handler(handler)

        return handler

    def _handle(self, context):
        skip_next = False
        try:
            self._handle_state = self.handle(context)
        except SkipNextChain:
            skip_next = True

        try:
            handler = self.get_next_handler(skip_next)
            self._propagate_handler(handler, context)
        except EndOfChain:
            pass

    def _propagate_handler(self, handler, context):
        handler._handle(context)

    def get_next_handler(self, skip_one=False) -> "AbstractHandler":
        next = self.next
        if skip_one:
            return next.get_next_handler(False)
        return next

    def execute(self, context):
        """Method to start the chain from start"""
        handler = self._ctx.get_root_handler()
        self._propagate_handler(handler, context)


class ConditionalHandler(AbstractHandler):

    def __init__(self, *, success=None, failure=None):
        self.success_handler = success
        self.failure_handler = failure

        assert isinstance(success, AbstractHandler) or isinstance(
            failure, AbstractHandler
        )

    def handle(self, context) -> Optional[bool]:
        raise MethodNotOverriddenException(
            f"`handle` method is not overridden in {self.__class__.__name__}."
        )

    def get_next_handler(self, skip_one=False) -> AbstractHandler:
        handler = self.success_handler if self._handle_state else self.failure_handler
        if handler:
            start = self._queue_position + 1
            for h in handler._ctx.peek_queue():
                self._ctx.register_handler(h, start)
                start += 1
        return super().get_next_handler(skip_one=False)
