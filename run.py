from base import AbstractHandler, ConditionalHandler, SkipNextChain


class CheckRecordExistsHandler(ConditionalHandler):

    def handle(self, context):
        print("I am checking some condition to invoke nested handler")
        # Return True or False based on some condition
        return context["sample_is_success"]  # or False based on actual condition


# Simple handlers that perform some action
class RecordGeneratedHandler(AbstractHandler):
    def handle(self, context):
        print("Record generated successfully.")


class ValidationHandler(AbstractHandler):
    def handle(self, context):
        print("I am validating")


class RecordCreationHandler(AbstractHandler):
    def handle(self, context):
        print("I am starting record creation process")


class NewRecordCreationHandler(AbstractHandler):

    def handle(self, context):
        print("Creating new record")


class CheckScheduledCustomerHandler(AbstractHandler):
    def handle(self, context):
        print("I am checking customer is scheduled or not")


class UpdateRecordCreationHandler(AbstractHandler):
    def handle(self, context):
        print("Updating existing record")
        raise SkipNextChain


if __name__ == "__main__":

    # Create handlers
    scheduled = CheckScheduledCustomerHandler()
    update_record = UpdateRecordCreationHandler() 

    pre_check = ValidationHandler()  # 1
    new_record_sub_chain = NewRecordCreationHandler()  # a sub chain  # false
    update_record_sub_chain = scheduled.set_next(update_record)  # a sub chain  # true

    # Chain branching handler
    record_creation = CheckRecordExistsHandler(
        success=update_record_sub_chain, failure=new_record_sub_chain
    )

    record_generated = RecordGeneratedHandler()

    # Build the main chain
    link_generation = pre_check.set_next(record_creation).set_next(record_generated)

    # Execute the main chain with passing context
    # change the bool to False or True to see the difference in the output
    link_generation.execute(context={"sample_is_success": True})
